### Open Street Map Data of Russia-Ukraine Conflict ###
Project director:
* Richard Sowers
    * <r-sowers@illinois.edu>
    * <https://publish.illinois.edu/r-sowers/>

Copyright 2023 University of Illinois Board of Trustees. All Rights Reserved. Licensed under the MIT license

### Explanation:
This repo contains the results of several [Illinois Geometry Laboratory](https://math.illinois.edu/research/igl) projects investigating how the [Russian Invasion of Ukraine](https://en.wikipedia.org/wiki/Russian_invasion_of_Ukraine) is manifested in [Open Street Map](https://en.wikipedia.org/wiki/OpenStreetMap). The project centers around data and visualization of changes to Open Street Map in various _engagements_.

* Summer 2023
  * Commit: https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/tree/c4de6e0858ccbbaaaef8ad87c71fc8921995550a
  * Members
    * [Venkata Sai Narayana Bavisetty](https://www.linkedin.com/in/venkata-sai-narayana-bavisetty-62814476/) (graduate mentor)
    * [Yuxuan Chen](https://www.linkedin.com/in/yuxuan-chen-18560b231/)
    * [Paul Ge](https://www.linkedin.com/in/paul-ge-47861a233/)
    * [Yujie Miao](https://www.linkedin.com/in/yujie-miao-9137a5257/)
    * [Yiqian Zhang](https://www.linkedin.com/in/yiqianzhang1004/)
    * [Runlin Zheng](https://www.linkedin.com/in/runlin-zheng-5720b9195/)

* Spring 2023
  * Commit: https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/tree/12c88d468b21f3e9eb06ed03311bb95fa6320b4c
  * Members
    * [Alyson Chu](https://www.linkedin.com/in/alyson-chu-971100220/)
    * [Wenqi Zeng](https://www.linkedin.com/in/wenqi-zeng947/)
    * Luke Peng
    * [Srikar Annapragada](https://www.linkedin.com/in/srikar-annapragada-732221220/)

* Fall 2022 
  * Commit: https://gitlab.engr.illinois.edu/r-sowers/fall2022igl
  * Members
    * Zihan Zhou
    * [Zijun Yu](https://www.linkedin.com/in/zijun-yu-b41a481a2/)
    * [Adrian Russo](https://www.linkedin.com/in/adrian-russo-04b603225/)
    * Jung Soo Park (mentor)

### ukraine_admin_boudaries.csv:
This dataset includes the following information:
* Administrative Region: The name of the administrative region within Ukraine.
* Administrative Division: The specific division or district within the administrative region.
* Type: The type of administrative division, such as oblast, raion, or municipality.
* Population: The estimated population of the administrative division.
* Area (km²): The area of the administrative division in square kilometers.
<br>

### Engagements:
This folder includes a collection of conflicts that occurred during the war, accompanied by visualizations we have created. A number of visualiztion files were built on the work of the [Spring 2023 group](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/tree/). <br>

Each folder contains:
* _heatmap_animation.mp4_, which visualizes the geography of changes in osm slightly before and after the onset of the engagement.
* _wordcloud_animation.mp4_ visualizes the evolution of the tags of relevant changesets before and after the onset of the engagement.
* _heatmap_wordcloud_combined.mp4_, which is a combination of the first two animations, presenting both the geographical changes and the evolution of tags in a side-by-side format.
* _comparison_map.png_ visualizes the differences of changes between any two particular months for a selected military engagement. The corresponding _comparison_map.html_ can be zoomed in to see more details.
* _changesets_histogram.png_, which visualizes the trend of changeset's number before, during, and after the engagement. The histogram that shows changeset's number is connected with actual dates that is aggregated by week.

The information of conflicts are based on [engagements.json](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/engagements.json). This records name, url, location, Bounding Box Corners, start date, end date, and result of conflicts in Ukraine.<br>

This is an example of the [Avdiivka Engagement Folder](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/tree/main/Engagements/Avdiivka) with its corresponding files [heatmap_animation.mp4](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/Engagements/Avdiivka/heatmap_animation.mp4), [wordcloud_animation.mp4](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/Engagements/Avdiivka/wordcloud_animation.mp4), [heatmap_wordcloud_combined.mp4](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/Engagements/Avdiivka/heatmap_wordcloud_combined.mp4), [comparison_map.png](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/Engagements/Avdiivka/comparison_map.png), and [changesets_histogram.png](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/Engagements/Avdiivka/changesets_histogram.png).<br>
<br>

### osm_analysis.py:
This python file contains functions useful to scrape, store, visualize and analyse Open Street Map data. Currently, this file contains functions to scrape data from OSM API, upload specific data to MongoDB server, plot heatmaps, create animations and generate wordclouds. 

* The python script [process.py](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/process.py) is called to scrape and upload data onto the MongoDB server. It uses functions from [osm_analysis.py](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/osm_analysis.py) and [upload_mongodb.py](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/upload_mongodb.py).
<br>


### diff_file_analysis (Unfinished work):
This exploratory folder contains tools to understand the changes to nodes in a particular region.
* The notebook [node_history.py](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/diff_file_analysis/node_history.py) gives us a way to retrieve the node history given the id of the node. An example node history txt file for node id 10864855641 is shown in [node_history_10864855641.txt](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/diff_file_analysis/avdiivka/node_history_10864855641.txt).<br>

* The notebook [diff_scraper.ipynb](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/diff_file_analysis/diff_scraper.ipynb) reads the OSM hourly diff data for the whole world which is in XML format. It then converts it to a readble JSON format. Then it extracts the JSON for the relevant region. For example, we used this notebook to compile all changes in the city Avdiivka into [avidiivka_diff.json](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/diff_file_analysis/avdiivka_diff.json).<br>
<br>

### Presentations:
* The presentation [ukraine_data_final_presentation.pptx](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/ukraine_data_final_presentation.pptx) explains the progress made by the IGL team for Summer 2023.
* Additionally, the presentation [ukraine_data_mid_presentation.pptx](https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/ukraine_data_mid_presentation.pptx) offers an overview of the IGL team's progress at the midway point of the project during Summer 2023.<br>
<br>

### References:
* Wikipedia - list of military engagements
  * This [Wikipedia list](https://en.wikipedia.org/wiki/List_of_military_engagements_during_the_Russian_invasion_of_Ukraine) is a list of military engagements during the Russian invasion of Ukraine encompassing land, naval, and air engagements as well as campaigns, operations, defensive lines and sieges.<br>
<br>

* ISW - Ukraine Conflict updates
  * This [webpage](https://www.understandingwar.org/backgrounder/ukraine-conflict-updates) collects updates on the conflict in Ukraine from the Institute for the Study of War (ISW) and Critical Threats Project (CTP). Here are the links to [ISW](https://www.understandingwar.org/) and [CTP](https://www.criticalthreats.org/).<br>
<br>

* Wikipedia - List of cities in Ukraine
  * This [Wikipedia list](https://en.wikipedia.org/wiki/List_of_cities_in_Ukraine) is a list of cities in Ukraine.
<br>

* OSHDB - OpenStreetMap History Data Analysis
  * This [Github link](https://github.com/giscience/oshdb) provides a potential method of obtaining deleted nodes of an engagement within timestamps, which current OSM API cannot provide.
<br>


