'''
Professor Richard Sowers
Members: Venkata Sai Narayana Bavisetty, Yuxuan Chen, Paul Ge, Yujie Miao, Yiqian Zhang, Runlin Zheng
'''

from pymongo import MongoClient
from dotenv import load_dotenv
import urllib
import json
import os
import time
import osmapi
import pandas as pd


def process_record(record, cl_elements, cutoff_date):  #Yuxuan
    """
    Processes a record by querying the Overpass API, filtering the data based on the cutoff date.
    Inserts the filtered data into the specified collection.
    """
    overpass_query_url = record['api_call']
    with urllib.request.urlopen(overpass_query_url) as url:
        data = json.load(url)
        filtered_data = [element for element in data["elements"] if element["timestamp"] >= cutoff_date]
        filtered_data.sort(key=lambda x: x['timestamp'], reverse=True)
    if len(filtered_data) > 0: cl_elements.insert_many(filtered_data)
    return


def mongodb_upload(fname: str, engagement_name: str, credentials):  #Yuxuan
    """
    Uploads data to MongoDB by processing scraper instructions from the specified 'fname' file.
    This function will generate a database with corresponding 'engagement_name'.
    credentials[0] = CUTOFF_DATE
    credentials[1] = ENV_PATH
    credentials[2] = ENV_USERNAME
    credentials[3] = ENV_PASSWORD
    It will create a collection named ELEMENTS.
    """
    load_dotenv(credentials[1])
    username = os.getenv(credentials[2])
    password = os.getenv(credentials[3])
    connection_string = f"mongodb+srv://{username}:{password}@ukraine-data.4otff.mongodb.net/test"
    client = MongoClient(connection_string)
    existing_database_names = client.list_database_names()
    if engagement_name in existing_database_names:
        print(f"{engagement_name} already exists in the database. Skipping...")
        return
    with open(fname, "r") as f:
        data=json.load(f)
    db = client[engagement_name]
    cl_elements = db["ELEMENT"]
    processed_flag = False
    sleep_flag = 0
    for n, record in enumerate(data):
        if record["status"] == "done": continue
        process_record(record, cl_elements, credentials[0])
        processed_flag = True
        data[n]["status"] = "done"
        sleep_flag += 1
        if (sleep_flag >= 5):
            sleep_flag = 0
            time.sleep(1)
    client.close()
    if processed_flag == False: print("no records processed")
    else: 
        with open(fname, "w") as f: 
            json.dump(data, f, indent = 2)
    f.close()
    print(f"{engagement_name} ELEMENT success")
    mongodb_upload_changeset(connection_string, engagement_name)
    print("updated database names")
    return


def mongodb_upload_changeset(connection_string, engagement_name):
    """
    Uploads changeset information to MongoDB by retrieving and inserting changeset data from ELEMENT.
    Requires database having ELEMENT collection.
    """
    api = osmapi.OsmApi()
    client = MongoClient(connection_string)
    collection = client[engagement_name]["ELEMENT"]
    cursor = collection.find({}, {'_id': 0, 'changeset': 1})
    cursor_list = list(cursor)
    changesets = [item['changeset'] for item in cursor_list]
    unique_changesets = pd.unique(changesets)
    changeset_info = []
    for cs in unique_changesets:
        item = api.ChangesetGet(cs)
        changeset_info.append(item)
    changeset_collection = client[engagement_name]["CHANGESET"]
    changeset_collection.insert_many(changeset_info)
    print(f"{engagement_name} CHANGESET success")
    return
