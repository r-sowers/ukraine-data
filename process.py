'''
Professor Richard Sowers
Members: Venkata Sai Narayana Bavisetty, Yuxuan Chen, Paul Ge, Yujie Miao, Yiqian Zhang, Runlin Zheng
'''

import osm_analysis as oa
import sys
import json

ENGAGEMENT_CONFIG = "./engagements.json"

# Main runs when this python script is called.
# To run the script, do:    python process.py [-flag] [name1] [name2] ...
# Example1:                 python process.py -m Avdiivka
# Example2:                 python process.py -m Avdiivka Romny
if __name__ == "__main__":

    if len(sys.argv) <= 2:
        print("Incorrect usage")
        print("Please do: python process.py [-flag] [name1] [name2] ...")
        print("Example1: python process.py -m Avdiivka")
        print("Example2: python process.py -m Avdiivka Romny")
        sys.exit(0)

    engagement_list = []
    engagement_object = []
    data = []
    with open(ENGAGEMENT_CONFIG, 'r') as json_file:
        data = json.load(json_file)
        for info in data:
            engagement_list.append(info['city'])
    if sys.argv[1] == "-m":
        for engagement in sys.argv[2:]:   
            try:
                name = (oa.parse_input(engagement, engagement_list))[0]
                for info in data:
                    if name == info['city']:
                        engagement_object = oa.engagement_bbox_setup(info)
                        break
                oa.scrape_upload(engagement_object)
            except:
                print(f"Error: {engagement} failed to upload to mongodb. Are you sure the Folder exists?\n")
