'''
Professor Richard Sowers
Members: Venkata Sai Narayana Bavisetty, Yuxuan Chen, Paul Ge, Yujie Miao, Yiqian Zhang, Runlin Zheng
'''
import pandas as pd
import numpy as np
import itertools
import json
import upload_mongodb
import os
import matplotlib.dates as mdates
import matplotlib
from matplotlib.ticker import FuncFormatter
from pymongo import MongoClient
import datetime
import dateutil.parser
import folium
import folium.plugins as plugins
from collections import defaultdict, OrderedDict
import time
from selenium import webdriver
import glob
import shutil
import matplotlib
from matplotlib.animation import FuncAnimation
from matplotlib.image import imread
import difflib
from IPython.display import display
import numpy
from dotenv import load_dotenv
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from collections import OrderedDict
from selenium import webdriver
from folium.plugins import HeatMap
import shutil
import statistics
import wordcloud
import itertools
import pytz
import cv2
import ipywidgets
import IPython.display
import emoji
from PIL import Image
CUTOFF_DATE = "2021-11-01"                  # We focus on dates after 2021-11-01. Change CUTOFF_DATE if necessary.
ENV_PATH = "./.env"                         # The path of env. Change ENV_PATH if necessary.
ENV_USERNAME = "DB_USERNAME"                # env variables for logging into MongoDB. Change ENV_USERNAME if necessary.
ENV_PASSWORD = "DB_PASSWORD"                # env variables for logging into MongoDB. Change ENV_PASSWORD if necessary.
INFO = [CUTOFF_DATE, ENV_PATH, ENV_USERNAME, ENV_PASSWORD]


"""This file contains all the functions we can use for analysis of OSM data"""
class engagement_bbox:  #Yuxuan
    """This class contains the engagement name and the geographical bounding box.
    It also contains the start and end date of the conflict.
    Sometimes there might be more than one bounding box because an engagement can happen in multiple cities at once"""
    def __init__(self, general_name: str, name: str, north: list, south: list, east: list, west: list):
        """This function initializes the name of the engagement and its bounding box/es"""
        self.general_name = general_name
        self.name = name
        self.north = north
        self.south = south
        self.east = east
        self.west = west
        assert len(self.north) == len(self.south) == len(self.east) == len(self.west), "north south east west lists must have same size"
        for case in range(len(self.north)):
            assert (self.north)[case] > (self.south)[case], "north bound must be greater than south bound"
            assert (self.east)[case] > (self.west)[case], "east bound must be greater than west bound"


def engagement_bbox_setup(engagement):  #Yuxuan
    """Sets up the engagement_bbox Object.
    Requires correct json object with correct information. Below is an example of needed information.
    {   "name": "Battle of Avdiivka",
        "city": "Avdiivka",
        "Bounding_Box_Corners_0": [{"lat": "48.1805275","lon": "37.6852875"},{"lat": "48.1069333","lon": "37.8078926"}]}"""
    try:
        general_name = engagement["name"].replace(" ", "_")
        print("Engagement name:", general_name)
        name = engagement["city"]
        north, south, east, west = [[] for _ in range(4)]
        for key in engagement.keys():
            if key.startswith("Bounding_Box_Corners_"):
                north.append(engagement[key][0]['lat'])
                west.append(engagement[key][0]['lon'])
                south.append(engagement[key][1]['lat'])
                east.append(engagement[key][1]['lon'])
        print("Engagement geobounds:", north, south, east, west)
        engagement_object = engagement_bbox(general_name, name, north, south, east, west)
        return engagement_object
    except:
        print("Error: config json object not found/incorrect.\n")
        return None


def heatmap_json(json_input, engagement_name, save=True, latitude = "lat", longitude = "lon"):  # Yiqian
    """This function takes in a JSON file and returns the heat map.
    Args: JSON file f.json . Make sure you check it is a valid JSON file.
    Output: Outputs f_heatmap.html and f_heatmap.png"""

    # Load JSON data from file or directly use input list
    if type(json_input) == str:
        with open(json_input) as f:
            data = json.load(f)
    elif type(json_input) == list:
        data = json_input

    # Initialize coordinate variables
    lat = latitude
    lon = longitude
    past_data = data
    data = []

    # Collect elements that have both latitude and longitude
    for element in past_data:
        if lat in element and lon in element:
            data.append(element)

    # Function to calculate average geographic coordinates
    def get_geo_coordinate(data):
        lati = []
        long = []
        for element in data:
            lati.append(float(element[lat]))
            long.append(float(element[lon]))
        city_coordinate = [numpy.average(lati), numpy.average(long)]
        return city_coordinate

    # Sort data by timestamp
    def sorter(kv):
        k, _ = kv
        return k

    city_coordinate = get_geo_coordinate(data)

    # Function to get start of the week for a date
    def get_start_of_week(date): 
        date = dateutil.parser.parse(date).date()
        start_of_week = date - datetime.timedelta(days=date.weekday())
        return start_of_week

    # Filter data based on timestamp
    war_data = [item for item in data if item["timestamp"] >= CUTOFF_DATE]
    coords = {}

    # Organize coordinates by week
    for item in war_data:
        if item.get(lat) and item.get(lon):
            start_of_week = get_start_of_week(item["timestamp"])
            coords[start_of_week] = coords.get(start_of_week, []) + [[item[lat], item[lon]]]

    # Sort coordinates by date
    sorted_coords = OrderedDict(sorted(coords.items(), key=sorter))

    # Prepare for heatmap generation
    time_index = ["Week of {0:s}".format(str(sow)) for sow in sorted_coords.keys()]
    city_lat, city_lon = city_coordinate

    # Initialize map
    city_vis = folium.Map(location=[city_lat, city_lon], zoom_start=13, zoom_control=False)
    folium.TileLayer('stamentoner').add_to(city_vis)

    # Add heatmap layer to map
    hm = plugins.HeatMapWithTime(data=list(sorted_coords.values()),
                                 index=time_index, 
                                 radius=10,
                                 max_opacity=0.9)
    hm.add_to(city_vis)

    # Save map to file if requested
    # Save map to file if requested
    if save:
        directory = engagement_name.title().replace(" ", "_")
        os.makedirs(directory, exist_ok=True)  # create directory if it does not exist

        html_name = os.path.join(directory, "heatmap.html")
        city_vis.save(html_name)

        # Convert html to png using Selenium webdriver
        fileURL = f"file://{os.getcwd()}/{html_name}"
        image_name = os.path.join(directory, "heatmap.png")
        delay = 5
        browser = webdriver.Chrome()
        browser.get(fileURL)
        time.sleep(delay)
        browser.save_screenshot(image_name)
        browser.quit()

    return city_vis



def scraper(engagement: engagement_bbox, path: str="null"):  #Yuxuan
    """This takes in an object of engagement_bbox and collects the data from overpass_api. 
    Args: An object of class engagement_bbox, path(optional) for where to save the JSON.
    Output: Outputs the JSON file to the location indicated or just returns it"""
    fname = f"./Engagements/{engagement.name}/{engagement.name}_scraper.json"
    if path != "null":
        if path[-1] != "/":
            path = path + "/"
        fname = f"{path}{engagement.name}_scraper.json"
    if os.path.exists(fname):
        print("File name already exists, please use another path.")
        return
    Nx = 10
    Ny = 10
    data_api = []
    cutoff = "2021-11-01T00:00.00Z"
    for case in range(len(engagement.north)):
        (x_min,x_max) = engagement.west[case], engagement.east[case]
        (y_min,y_max) = engagement.south[case], engagement.north[case]
        xlist = np.linspace(float(x_min), float(x_max), num=Nx+1)
        ylist = np.linspace(float(y_min), float(y_max), num=Ny+1)
        xintervals = [(xstart,xstop) for xstart,xstop in zip(xlist[:-1],xlist[1:])]
        yintervals = [(ystart,ystop) for ystart,ystop in zip(ylist[:-1],ylist[1:])]
        squares = list(itertools.product(xintervals, yintervals))
        for square in squares:
            element_api=f"https://www.overpass-api.de/api/interpreter?data=[out:json];area(297948617);nwr({square[1][0]},{square[0][0]},{square[1][1]},{square[0][1]});area._(newer:%22{cutoff}%22);(._;%3E;);out%20meta;"
            data_api.append({"api_call": element_api, "status": "undone"})
    with open(fname, "w") as f:
        json.dump(data_api, f , indent=2)
    return fname


def upload_json(fname: str, database_name: str, credentials=INFO):  #Yuxuan
    """This uploads the JSON file to MongoDB.
    Args: An 'fname' JSON file, the engagement_bbox object, and MongoDB credentials from a .env file."""
    print(f"Starting MongoDB upload for {database_name}")
    database_name = database_name.replace(" ", "_")
    upload_mongodb.mongodb_upload(fname, database_name, credentials)
    print(f"Ending MongoDB upload for {database_name}\n")


def parse_input(user_input: str, match_list: list):  # Yiqian
    """Takes a string and finds the correct engagement by parsing it.
    For example, if I input avdivka it should output ['Avdiivka'].
    Args: String engagement_name, list engagement lists.
    Output: a list of matched engagement name."""

    # Initialize cutoff ratio for difflib matching
    cf = 0.0

    # Get a list of close matches to the user input from the match list
    match = difflib.get_close_matches(user_input, match_list, n = 5, cutoff = cf)

    # If more than one match is found, incrementally increase the cutoff ratio to narrow down the matches
    while len(match) > 1:
        cf = cf + 0.01
        match = difflib.get_close_matches(user_input, match_list, n = 5, cutoff = cf)

    # Return the list of matched engagement names
    return match



def scrape_upload(engagement: engagement_bbox, credentials=INFO):  #Yuxuan
    """This takes in an object of engagement_bbox and collects the data from overpass_api and uploads to MongoDb.
    Args: An object of class engagement_bbox and .env file for credentials.
    Output: Uploads the JSON files to engagement_bbox.name folder on MongoDB."""
    fname = scraper(engagement)
    upload_json(fname, engagement.name, credentials)
    if os.path.exists(fname):
        os.remove(fname)


def open_input(input):  # Yiqian
    """Takes a string (representing a path to a JSON file) or a list as an input.
    If the input is a string, the function treats it as a path to a JSON file and loads the data from this file.
    If the input is a list, the function directly uses it.
    Args: input (a string or a list).
    Output: data from the JSON file or the input list."""

    # If the input is a string, treat it as a path to a JSON file
    if type(input) == str:
        with open(input) as f:
            data = json.load(f)
    # If the input is a list, directly use it
    elif type(input) == list:
        data = input
    # Return the loaded data or input list
    return data



def visualize_json(json_input, engagement_name): #Yiqian
    """This takes in a JSON file and creates an mp4 animation of the heatmap.
    Args: JSON file
    Output: An mp4 animation."""

    # Use the open_input function to read the JSON file or list
    data = open_input(json_input)

    # Check if the input data is valid. If not, return an error message
    if valid_json(data) == False:
        return("file input is not valid")
    
    # Define the latitude and longitude
    lat = "lat"
    lon = "lon"
    
    # Create a new list containing only data points with valid latitudes and longitudes
    past_data = data
    data = []
    for element in past_data:
        if lat in element and lon in element:
            data.append(element)

    def get_geo_coordinate(data):
        lati = []
        long = []
        for element in data:
            lati.append(float(element[lat]))
            long.append(float(element[lon]))
        city_coordinate = [numpy.average(lati), numpy.average(long)]
        return city_coordinate

    def get_month(date):
        date = dateutil.parser.parse(date).date()
        return date.replace(day=1)

    war_data = [item for item in data if item["timestamp"] >= CUTOFF_DATE]
    
    coords={}
    for item in war_data:
        if item.get(lat) and item.get(lon):
            month=get_month(item["timestamp"])
            coords[month]=coords.get(month,[])+[[item[lat],item[lon]]]
    
    city_lat, city_lon = get_geo_coordinate(data)
    
    folder_name = "./heatmaps"
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

    for month, month_data in coords.items():
        city_vis = folium.Map(location=[city_lat, city_lon], zoom_start=13, zoom_control=False)
        folium.TileLayer('stamentoner').add_to(city_vis)
        folium.plugins.HeatMap(month_data, radius=10, max_opacity=0.9).add_to(city_vis)
        html_name = f"{folder_name}/heatmap.html"
        city_vis.save(html_name)
        fileURL = f"file://{os.getcwd()}/{html_name}"
        image_name = f"{folder_name}/Month_of_{month}.png"
        delay = 5
        browser = webdriver.Chrome()
        browser.get(fileURL)
        time.sleep(delay)
        browser.save_screenshot(image_name)
        browser.quit()
        os.remove(html_name)
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ims = []
    image_files = sorted([f"{folder_name}/{img}" for img in os.listdir(folder_name) if img.endswith(".png")])
    start_date_str = image_files[0].split("/")[-1].replace(".png", "").replace("Month_of_", "")
    start_date = datetime.datetime.strptime(start_date_str, "%Y-%m-%d").strftime("%B %Y")
    for image_file in image_files:
        img_np = plt.imread(image_file)
        date_str = image_file.split("/")[-1].replace(".png", "").replace("Month_of_", "")
        date = datetime.datetime.strptime(date_str, "%Y-%m-%d").strftime("%B %Y")
        title = plt.text(0.5, 1.01, f"{engagement_name[0]} \n Engagement started: {start_date} \n {date}", horizontalalignment='center', verticalalignment='bottom', transform=ax.transAxes)
        ims.append([plt.imshow(img_np), title])
    ani = animation.ArtistAnimation(fig, ims, interval=2000, blit=False, repeat_delay=1000)
    writer = animation.FFMpegWriter(fps=1, metadata=dict(artist='Me'), bitrate=1800)
    ani.save("heatmap_animation.mp4", writer=writer)
    plt.show()

    shutil.rmtree(folder_name)



def scraper_diff():
    """Takes an object of class engagement_bbox and outputs a JSON file containing the changes.
    This reuires downloading the files from the URL so make sure you take connection issues into account.
    For example, using wget etc.
    Args: An object of class engagement_bbox and location(optional)
    Output: A JSON file stored in the location indicated or just return it.
    """
    pass
    """your code goes here"""


def get_file(name_of_database: str, collection: str):  # Yiqian
    """This function connects to a MongoDB database and fetches data from a specified collection.
    
    Args:
        name_of_database (str): The name of the database to connect to.
        collection (str): The name of the collection to fetch data from.

    Returns:
        data: List of documents (data) in the specified MongoDB collection.

    """
    # Load environment variables from the .env file
    load_dotenv(".env")

    # Fetch the username and password for the MongoDB server from the environment variables
    USERNAME = os.getenv("DB_USERNAME")
    PASSWORD = os.getenv("DB_PASSWORD")

    # Define the connection string for the MongoDB server
    CONNECTION_STRING = f"mongodb+srv://{USERNAME}:{PASSWORD}@ukraine-data.4otff.mongodb.net/test"

    # Create a MongoDB client using the connection string
    client = MongoClient(CONNECTION_STRING)

    # Parse the input database name to match one in the list of database names
    database_name = (parse_input(name_of_database, client.list_database_names()))[0]

    # Ensure the collection name is in uppercase
    collection_name = str(collection.upper())

    # Fetch all documents from the specified database and collection
    data = list(client[database_name][collection_name].find({}))

    # Return the fetched data
    return(data)



def heatmap_engagement(database_name, collection, savepng_html = True):  #Yiqian
    """Takes an object of class engagement_bbox, then get the file from MongoDB. Finally generate the heatmap of the retrived JSON file.
    Args: An object of class engagement_bbox
    OUtput: Return the enagement_name/heatmap.html
    """
    return(heatmap_json(get_file(database_name,collection), save=savepng_html))


def compare_heatmap_json(json_1, json_2):  # Yiqian
    """ takes two JSON files or lists as input and generates a heatmap comparing the geographical data in the two JSONs. 
    It validates the data, extracts latitude and longitude, and calculates a geographical center to create a Folium map. 
    The function then generates two separate heatmaps from the input data and overlays them onto the map with different color schemes
    """
    # Open the JSON files or process the input lists
    data_1 = open_input(json_1)
    data_2 = open_input(json_2)

    # Validate the JSON data
    if valid_json(data_1) == False:
        return f"First input data is not valid"
    if valid_json(data_2) == False:
        return f"Second input data is not valid"
        
    # Extract only elements with 'lat' and 'lon' keys from the data
    data_1 = [element for element in data_1 if 'lat' in element and 'lon' in element]
    data_2 = [element for element in data_2 if 'lat' in element and 'lon' in element]

    # Define a function to calculate the geographic center of the coordinates
    def get_geo_coordinate(data_1, data_2):
        lati = []
        long = []
        for element in data_1:
            lati.append(float(element['lat']))
            long.append(float(element['lon']))
        for element in data_2:
            lati.append(float(element['lat']))
            long.append(float(element['lon']))
        city_coordinate = [numpy.average(lati), numpy.average(long)]
        return city_coordinate

    # Calculate the geographic center of the data
    city_coordinate = get_geo_coordinate(data_1, data_2)

    # Define a function to extract the coordinates from the JSON data
    def process_data(json_data):
        coords = []
        for item in json_data:
            if item['type'] == 'node':
                coords.append([item['lat'], item['lon']])
        return coords

    # Extract the coordinates from the JSON data
    coords_1 = process_data(data_1)
    coords_2 = process_data(data_2)

    # Create a Folium map centered on the calculated geographic center
    city_vis = folium.Map(location=city_coordinate, zoom_start=13, zoom_control=False)

    # Add the heatmaps of the two sets of data to the map with different color schemes
    HeatMap(coords_1, name="JSON 1 heatmap", gradient={0.2: 'red', 0.6: 'orange', 1: 'yellow'}).add_to(city_vis)
    HeatMap(coords_2, name="JSON 2 heatmap", gradient={0.2: 'blue', 0.6: 'cyan', 1: 'green'}).add_to(city_vis)

    # Add a control layer to the map to toggle the visibility of the heatmaps
    folium.LayerControl().add_to(city_vis)

    # Return the map
    return(city_vis)



def valid_json(json_list):  # Yiqian
    """This function takes a JSON file or a list and checks if it contains necessary fields to generate a heatmap.

    Args:
        json_list: A JSON file or a list.

    Returns:
        Returns True if JSON contains 'latitude', 'longitude' and 'timestamp' or 'lat', 'lon', and 'timestamp'. 
        Returns False otherwise.
    """
    # Open the JSON file or process the input list
    data = open_input(json_list)

    # Loop through each element in the JSON data
    for element in data:
        # Check if the necessary fields for a heatmap are present
        if "latitude" in element and "longitude" in element and "timestamp" in element:
            return True
        elif "lat" in element and "lon" in element and "timestamp" in element:
            return True

    # If none of the elements have the necessary fields, return False
    return False


# Interactive Map for Comparing Changes between Any Two Particular Months
# Select conflict area & any two months between the time range of the conflict
def compare_interactive_map(conflict_city: str, before_time: datetime, after_time: datetime):  #Runlin
    """E.g.  Battle of Siversk happened in "Donetsk" (Time Range: 2022/7 - 2022/9)
    Pick any time between: July 2022 - September 2022 for comparison.
    Example Arguments:
    conflict_city = "Donetsk" 
    before_time = datetime.date(2022, 7, 1)   datetime.date(year, month, day)
    after_time = datetime.date(2022, 8, 1)    datetime.date(year, month, day)"""
    
    # Retrieve the data from MongoDB
    past_data = get_file(conflict_city.lower(), 'ELEMENT')
    cur_data = past_data.copy()

    # Filter Data by Year and Month
    next_before = (pd.to_datetime(before_time) + pd.DateOffset(months = 1)).date()
    next_after = (pd.to_datetime(after_time) + pd.DateOffset(months = 1)).date()
    target1 = []
    for element in past_data:
        if element["timestamp"] >= str(datetime.date(before_time.year, before_time.month, before_time.day)) and element["timestamp"] < str(datetime.date(next_before.year, next_before.month, next_before.day)):
            target1.append(element)
    if len(target1) == 0:
        print(f"no interesting data in {before_time.year}.{before_time.month}")
    target2 = []
    for element in cur_data:
        if element["timestamp"] >= str(datetime.date(after_time.year, after_time.month, after_time.day)) and element["timestamp"] < str(datetime.date(next_after.year, next_after.month, next_after.day)):
            target2.append(element)
    if len(target2) == 0:
        print(f"no interesting data in {after_time.year}.{after_time.month}")
    past_data = sorted(target1, key = lambda i: i['timestamp'], reverse = True)
    cur_data = sorted(target2, key = lambda i: i['timestamp'], reverse = True)

    # Calculate latitude and longitude of changesets
    past_lon = [float(element.get('lon') or 0) for element in past_data if float(element.get('lat') or 0) != 0.0] 
    past_lat = [float(element.get('lat') or 0) for element in past_data if float(element.get('lat') or 0) != 0.0]
    past_mean_lon = statistics.mean(past_lon)
    past_mean_lat = statistics.mean(past_lat)
    cur_lon = [float(element.get('lon') or 0) for element in cur_data if float(element.get('lat') or 0) != 0.0]
    cur_lat = [float(element.get('lat') or 0) for element in cur_data if float(element.get('lat') or 0) != 0.0]
    cur_mean_lon = statistics.mean(cur_lon)
    cur_mean_lat = statistics.mean(cur_lat)

    # Plot map with filtered nodes/changesets for a particular city in Ukraine
    conflict_vis = folium.Map(location=[past_mean_lat, past_mean_lon], zoom_start = 12.5, zoom_control = False)
    before_latlon = [(float(i.get('lat') or 0), float(i.get('lon') or 0)) for i in past_data if float(i.get('lat') or 0) != 0.0]
    after_latlon = [(float(i.get('lat') or 0), float(i.get('lon') or 0)) for i in cur_data if float(i.get('lat') or 0) != 0.0]
    for coord in before_latlon:
        folium.Circle(location = [coord[0], coord[1]], color = '#0000ff', fill_color = 'blue', radius = 10 , fill_opacity = 1).add_to(conflict_vis)
    for coord in after_latlon:
        folium.Circle(location=[coord[0], coord[1]], color = '#d4260b', fill_color = 'red', radius = 10 , fill_opacity = 1).add_to(conflict_vis)
    minimap = plugins.MiniMap()
    conflict_vis.add_child(minimap)

    # Get official name and time period of the engagements
    engagement_info = json.load(open('engagements.json'))
    conflict_name = ''
    conflict_time = ''
    for engagement in engagement_info:
        if engagement['city'] == conflict_city:
            conflict_name = engagement['name']
            conflict_time = engagement['start_date'].split()[1]
            conflict_time = conflict_time[:-4] + ' ' + conflict_time[-4:]
            break

    # Add legend and title to the map
    if before_time.year == after_time.year and before_time.month == after_time.month:
        legend_html = f'''
            <div style="position: fixed; 
                        bottom: 50px; left: 50px; width: 140px; height: 70px; 
                        border:2px solid grey; z-index:9999; font-size:14px;
                        ">&nbsp; <span style="font-weight:bold; font-size:15px;">Time Period</span> <br>
                        &nbsp; <i class="fa fa-circle fa-sm" style="color:red"></i> {before_time.strftime('%B')} {before_time.strftime('%Y')} <br>
            </div>
            '''
        title_html = f'''
            <h3 style="
                text-align: center;
                margin-top: 5px;
                margin-bottom: 5px;
                font-size: 15px;
            ">{conflict_name} <br/> Engagement Started: {conflict_time} <br/> Comparison Map of Changes for {before_time.strftime('%B')} {before_time.strftime('%Y')}</h3>
        '''
    else: 
        legend_html = f'''
            <div style="position: fixed;
                        bottom: 20px; left: 50px; width: 140px; height: 70px; 
                        border:2px solid grey; z-index:9999; font-size:14px;
                        ">&nbsp; <span style="font-weight:bold; font-size:15px;">Time Period</span> <br>
                        &nbsp; <i class="fa fa-circle fa-sm" style="color:blue"></i> {before_time.strftime('%B')} {before_time.strftime('%Y')} <br>
                        &nbsp; <i class="fa fa-circle fa-sm" style="color:red"></i> {after_time.strftime('%B')} {after_time.strftime('%Y')} <br>
            </div>
            '''
        title_html = f'''
            <h3 style="
                text-align: center;
                margin-top: 5px;
                margin-bottom: 5px;
                font-size: 15px;
            ">{conflict_name} <br/> Engagement Started: {conflict_time} <br/> Comparison Map of Changes for {before_time.strftime('%B')} {before_time.strftime('%Y')} vs. {after_time.strftime('%B')} {after_time.strftime('%Y')}</h3>
        '''
    conflict_vis.get_root().html.add_child(folium.Element(legend_html))
    conflict_vis.get_root().html.add_child(folium.Element(title_html))

    # Save the interactive map into html file & png file
    ## Please do not close window until code is finished running!
    file = 'Engagements/' + conflict_city.title().replace(" ", "_")
    if not os.path.exists(file):
        os.makedirs(file)
    html_name = file + "/comparison_map.html"
    conflict_vis.save(html_name)
    delay = 5
    fileURL = f"file://{os.getcwd()}/{html_name}"
    browser = webdriver.Chrome()
    browser.get(fileURL)
    time.sleep(delay)
    browser.save_screenshot(file + '/comparison_map.png')


def covert_diff_tojson():
    """Takes an OSM diff file and converts it to a readable JSON file.
    Relevant notebook is:
    https://gitlab.engr.illinois.edu/r-sowers/ukraine-data/-/blob/main/diff_file_analysis/test.ipynb
    Args: Takes an unzipped XML diff file and location: optional
    Output: JSON file to the location specified or return the JSON file.
    """
    pass
    """your code goes here"""




def histogram_time_json(input, engagement_start, engagement_end, event_name):
    """Takes a JSON file with a tag time stamp and creates a pretty histogram of the number of entries in each time slot."""
    
    # Load the data from the JSON input file
    data = open_input(input)
    
    # Convert engagement start and end dates to datetime objects
    engagement_start_date = datetime.datetime.strptime(engagement_start, '%d %B %Y').date()
    
    if engagement_end == "ongoing":
        engagement_end_date = datetime.date.today()
    else:
        engagement_end_date = datetime.datetime.strptime(engagement_end, '%d %B %Y').date()
    
    # Convert the date format in the code to include the extra space before the year
    try:
        engagement_start_date = datetime.datetime.strptime(engagement_start, '%d %B %Y').date()
    except ValueError:
        engagement_start_date = datetime.datetime.strptime(engagement_start, '%d %B%Y').date()
    
    # Calculate the start and end dates for the extended range (10 weeks before and after the event)
    extended_start_date = engagement_start_date - datetime.timedelta(weeks=10)
    extended_end_date = engagement_end_date + datetime.timedelta(weeks=10)
    
    # Create a list to store the date part of the timestamp from each record
    time = []
    for element in data:
        # Convert the timestamp string to datetime object
        timestamp_str = element["timestamp"]
        datetime_obj = datetime.datetime.strptime(timestamp_str, '%Y-%m-%dT%H:%M:%SZ')
        # Extract the date object from datetime and append it to the list
        date_obj = datetime_obj.date()
        time.append(date_obj)

    # Filter the data to retain only records within the extended date range
    filtered_data = [element for element in data if
                     datetime.datetime.strptime(element["timestamp"], '%Y-%m-%dT%H:%M:%SZ').date() >= extended_start_date
                     and datetime.datetime.strptime(element["timestamp"], '%Y-%m-%dT%H:%M:%SZ').date() <= extended_end_date]

    # Create a list of dictionaries with the week number and changes count for each record
    week = [{'week_number': (datetime.datetime.strptime(record.get("timestamp"), '%Y-%m-%dT%H:%M:%SZ').date() - engagement_start_date).days // 7 + 1,
             'changes_count': 1} for record in filtered_data]

    # Initialize a dictionary with week numbers as keys and an initial value of 0
    d = {}
    for i in range((extended_end_date - extended_start_date).days // 7 + 1):
        d[i+1] = 0

    # Update the dictionary with counts of changesets for each week
    for item in week:
        if item.get("week_number") not in d:
            d[item.get("week_number")] = 0
        d[item.get("week_number")] += int(item.get("changes_count"))

    # Convert the dictionary to a pandas DataFrame for easier plotting
    df = pd.DataFrame(list(d.items()), columns=['week_number', 'total_changesets'])

    # Calculate the interval between displayed dates based on the number of weeks in the engagement period
    num_weeks = (engagement_end_date - engagement_start_date).days // 7 + 1
    date_interval = max(num_weeks // 4, 1)

    # Create a list of tick locations and labels for the x-axis
    x_ticks = df['week_number'][::date_interval].tolist()
    x_labels = [(engagement_start_date + datetime.timedelta(weeks=(x - 1))).strftime('%Y-%m-%d') for x in x_ticks]

    # Create a histogram of total changesets
    fig, ax = plt.subplots(figsize=(10, 6))

    # Plot the histogram for the event period (red bars)
    ax.bar(df.loc[df['week_number'].between((engagement_start_date - extended_start_date).days // 7 + 1,
                                             (engagement_end_date - extended_start_date).days // 7 + 1),
                  'week_number'],
           df.loc[df['week_number'].between((engagement_start_date - extended_start_date).days // 7 + 1,
                                             (engagement_end_date - extended_start_date).days // 7 + 1),
                  'total_changesets'],
           color='red', alpha=0.8, label='During Event')

    # Plot the histogram for the non-event period (blue bars)
    ax.bar(df.loc[~df['week_number'].between((engagement_start_date - extended_start_date).days // 7 + 1,
                                              (engagement_end_date - extended_start_date).days // 7 + 1),
                  'week_number'],
           df.loc[~df['week_number'].between((engagement_start_date - extended_start_date).days // 7 + 1,
                                               (engagement_end_date - extended_start_date).days // 7 + 1),
                  'total_changesets'],
           color='blue', alpha=0.6, label='Outside Event')

    # Customize the x-axis tick labels
    ax.set_xticks(x_ticks)
    ax.set_xticklabels(x_labels, rotation=45, ha='right')

    # Set the title and labels for the plot
    ax.set(xlabel="Weeks",
           ylabel="Number of Changesets",
           title=f"Weekly Number of Changesets\nEngagement name: {event_name}\nEngagement: {engagement_start} - {engagement_end}")

    # Add a legend
    ax.legend()

    # Add gridlines
    ax.grid(True, linestyle='--', alpha=0.5)

    plt.tight_layout()
    plt.savefig('changesets_histogram.png', bbox_inches='tight')
    plt.show()









def wordcloud_json(input):  #Yiqian
    """Takes in a JSON file and generates a wordcloud from the tags.

    Args:
        input: Path to the JSON file containing the data.

    Output:
        Interactive slider to display wordclouds for different years.
    """

    # Load the data from the JSON input file
    data = open_input(input)

    # Set the random seed for reproducibility
    SEED = 0

    # Initialize wordcloud object
    mywordcloud = wordcloud.WordCloud(random_state=SEED, relative_scaling=0.7)

    # Define function to visualize wordcloud by years
    def visualize_by_years(start_year_int, end_year_int):
        # Convert the input year to datetime format
        start_year = datetime.datetime(start_year_int, 1, 1, tzinfo = pytz.UTC)
        end_year = datetime.datetime(end_year_int, 1, 1, tzinfo = pytz.UTC)

        # Check if the start year is before end year
        assert end_year >= start_year, "start year must be before end year"

        # Filter the data for the specified year range
        subdata = [collection for collection in data if start_year < pd.to_datetime(collection["timestamp"], utc = True) <= end_year]

        # Extract the keys (words for the wordcloud) from the tags in the filtered data
        totalkeys = [list(collection["tags"].keys()) for collection in subdata if "tags" in collection]

        # Combine the keys into one list
        totalkeys_combined = list(itertools.chain.from_iterable(totalkeys))

        # Generate the wordcloud
        wc = mywordcloud.generate(" ".join(totalkeys_combined))

        # Plot the wordcloud
        plt.figure(figsize=(20,10), facecolor='k')
        plt.imshow(wc)
        title="{0:d} to {1:d}".format(start_year_int, end_year_int)
        plt.title(title, color="red", fontsize=50, y=-0.1)
        plt.axis("off")

    # Extract the years from the timestamp in the data
    time = [str.split(element['timestamp'], "-")[0] for element in data]

    # Find the range of years in the data
    yeary = int(min(time))
    range = int(max(time)) - int(min(time))

    # Define helper function for the interactive slider
    def helper(in_int):
        if in_int is None:  # Do nothing if in_int is None
            return
        year = yeary + in_int
        visualize_by_years(year, year+1)

    # Initialize the interactive slider
    time_control = ipywidgets.widgets.IntSlider(value=None, min=0, max=range, step=1, description='Time', readout=False)
    out = ipywidgets.widgets.interactive_output(helper, {'in_int': time_control})

    # Display the slider and the output
    IPython.display.display(time_control)
    IPython.display.display(out)



def wordcloud_save(input, engagement_name):  #Yiqian
    # Load the data from the JSON input file
    data = open_input(input)

    # Set the random seed for reproducibility
    SEED = 0

    # Initialize wordcloud object
    mywordcloud = wordcloud.WordCloud(random_state=SEED, relative_scaling=0.7)

    # Define function to visualize wordcloud by years
    def visualize_by_years(start_year_int, end_year_int):
        # Convert the input year to datetime format
        start_year = datetime.datetime(start_year_int, 1, 1, tzinfo = pytz.UTC)
        end_year = datetime.datetime(end_year_int, 1, 1, tzinfo = pytz.UTC)

        # Check if the start year is before end year
        assert end_year >= start_year, "start year must be before end year"

        # Filter the data for the specified year range
        subdata = [collection for collection in data if start_year < pd.to_datetime(collection["timestamp"], utc = True) <= end_year]

        # Extract the keys (words for the wordcloud) from the tags in the filtered data
        totalkeys = [list(collection["tags"].keys()) for collection in subdata if "tags" in collection]

        # Combine the keys into one list
        totalkeys_combined = list(itertools.chain.from_iterable(totalkeys))

        # Generate the wordcloud
        wc = mywordcloud.generate(" ".join(totalkeys_combined))

        # Plot the wordcloud
        plt.figure(figsize=(20,10), facecolor='k')
        plt.imshow(wc)
        plt.title(f"{start_year_int} to {end_year_int}", color="red", fontsize=50, y=1.05)
        plt.text(x=0.5, y=-0.08, s=engagement_name[0], fontsize=50, ha='center', va='bottom', transform=plt.gca().transAxes, color='red')
        plt.axis("off")
        name = f"{start_year_int} to {end_year_int}".replace(" ","_")
        # Save the figure in the 'wordcloud' directory with the title as the filename
        if not os.path.exists('wordcloud'):
            os.makedirs('wordcloud')
        plt.savefig(f'wordcloud/{name}.png')

    # Extract the years from the timestamp in the data
    time = [str.split(element['timestamp'], "-")[0] for element in data]

    # Find the range of years in the data
    yeary = int(min(time))
    year_range = int(max(time)) - int(min(time))  # Rename 'range' to 'year_range'

# Iterate over all the years and save the word cloud for each year
    for i in range(year_range+1):
        visualize_by_years(yeary+i, yeary+i+1)



def create_animation(input, engagement_name): #Yiqian
    # Get the list of files in the directory
    data = open_input(input)

    # Set the random seed for reproducibility
    SEED = 0

    # Initialize wordcloud object
    mywordcloud = wordcloud.WordCloud(random_state=SEED, relative_scaling=0.7)

    # Define function to visualize wordcloud by years
    def visualize_by_years(start_year_int, end_year_int):
        # Convert the input year to datetime format
        start_year = datetime.datetime(start_year_int, 1, 1, tzinfo = pytz.UTC)
        end_year = datetime.datetime(end_year_int, 1, 1, tzinfo = pytz.UTC)

        # Check if the start year is before end year
        assert end_year >= start_year, "start year must be before end year"

        # Filter the data for the specified year range
        subdata = [collection for collection in data if start_year < pd.to_datetime(collection["timestamp"], utc = True) <= end_year]

        # Extract the keys (words for the wordcloud) from the tags in the filtered data
        totalkeys = [list(collection["tags"].keys()) for collection in subdata if "tags" in collection]

        # Combine the keys into one list
        totalkeys_combined = list(itertools.chain.from_iterable(totalkeys))

        # Generate the wordcloud
        wc = mywordcloud.generate(" ".join(totalkeys_combined))

        # Plot the wordcloud
        plt.figure(figsize=(20,10), facecolor='k')
        plt.imshow(wc)
        plt.axis("off")
        name = f"{start_year_int} to {end_year_int} {engagement_name[0]}".replace(" ","_")
        # Save the figure in the 'wordcloud' directory with the title as the filename
        if not os.path.exists('process'):
            os.makedirs('process')
        plt.savefig(f'process/{name}.png')

    # Extract the years from the timestamp in the data
    time = [str.split(element['timestamp'], "-")[0] for element in data]

    # Find the range of years in the data
    yeary = int(min(time))
    year_range = int(max(time)) - int(min(time))  # Rename 'range' to 'year_range'

# Iterate over all the years and save the word cloud for each year
    for i in range(year_range+1):
        visualize_by_years(yeary+i, yeary+i+1)
    fig, ax = plt.subplots()

    # Get a sorted list of the wordcloud image files.
    image_files = sorted(glob.glob('process/*.png'))
    def split_filename(filename):
        # Remove the .png extension
        filename = filename.split("/")[1]

        name_without_extension = filename.split(".png")[0]

        # Split the filename at the underscores
        parts = name_without_extension.split("_")

        # The year range is the first two parts, and the engagement name is the rest
        year_range = parts[0] + " to " + parts[2]
        engagement_name = " ".join(parts[3:])

        return year_range, engagement_name

    # Create a new figure
    # A list to store our images
    imgs = []

    # Read each image file and add it to imgs
    for image_file in image_files:
        img = imread(image_file)
        year_range, engagement_name = split_filename(image_file)
        title = plt.text(0.5, 1.01, f"{engagement_name} \n {year_range}", 
                         horizontalalignment='center', 
                         verticalalignment='bottom', 
                         transform=ax.transAxes)
        im = plt.imshow(img, animated=True)
        plt.axis('off')  # don't show axes for images
        imgs.append([im, title]) 

    # Create the animation
    ani = animation.ArtistAnimation(fig, imgs, interval=1000, blit=True, repeat_delay=1000)

    # Save the animation
    ani.save("wordcloud_animation.mp4", writer='ffmpeg')

    # Now that we have our animation, we can delete the wordcloud folder.
    shutil.rmtree('process')


#This part is used to create animation of combined heatmap(left) and wordcloud(right) You shoud use create_combined_heatmap_wordcloud function directly
def create_animations(input, engagement_name): #Yiqian
    # Get the list of files in the directory
    data = open_input(input)
    lat = "lat"
    lon = "lon"
    data = [element for element in data if "tags" in element and lat in element and lon in element and element["timestamp"] >= CUTOFF_DATE]

    # Set the random seed for reproducibility
    SEED = 0

    # Initialize wordcloud object
    mywordcloud = wordcloud.WordCloud(random_state=SEED, relative_scaling=0.7)

    # Define function to visualize wordcloud by months
    def visualize_by_months(start_year_int, start_month_int, end_year_int, end_month_int):
        # Convert the input year to datetime format
        start_time = datetime.datetime(start_year_int, start_month_int, 1, tzinfo=pytz.UTC)
        end_time = datetime.datetime(end_year_int, end_month_int, 1, tzinfo=pytz.UTC)

        # Check if the start year is before end year
        assert end_time >= start_time, "start year must be before end year"

        # Filter the data for the specified year range
        subdata = [collection for collection in data if start_time < pd.to_datetime(collection["timestamp"], utc=True) < end_time]

        # Extract the keys (words for the wordcloud) from the tags in the filtered data
        totalkeys = [list(collection["tags"].keys()) for collection in subdata if "tags" in collection]

        # Combine the keys into one list
        totalkeys_combined = list(itertools.chain.from_iterable(totalkeys))

        # If there are no words, then we skip this time period
        if len(totalkeys_combined) == 0:
            return

        # Generate the wordcloud
        wc = mywordcloud.generate(" ".join(totalkeys_combined))

        # Plot the wordcloud
        plt.figure(figsize=(20,10), facecolor='k')
        plt.imshow(wc)
        plt.axis("off")  
        name = f"Month_of_{start_year_int}-{start_month_int}-01"
        # Save the figure in the 'wordcloud' directory with the title as the filename
        if not os.path.exists('process'):
            os.makedirs('process')
        plt.savefig(f'process/{name}.png')


    # Extract the years and months from the timestamp in the data
    time = [str.split(element['timestamp'], "-")[0:2] for element in data if "tags" in element and lat in element and lon in element]

    # Find the range of years and months in the data
    start_year, start_month = map(int, min(time))
    end_year, end_month = map(int, max(time))

    # Get the number of months between the start and end dates
    num_months = (end_year - start_year) * 12 + (end_month - start_month)

    # Iterate over all the months and save the word cloud for each month
    # Iterate over all the months and save the word cloud for each month
    for i in range(num_months + 1):
        year = start_year + i // 12
        month = start_month + i % 12
        if month > 12:
            month -= 12
            year += 1
        next_year = year if month < 12 else year + 1
        next_month = month + 1 if month < 12 else 1
        visualize_by_months(year, month, next_year, next_month)

    
    fig, ax = plt.subplots()

    # Get a sorted list of the wordcloud image files.
    image_files = sorted(glob.glob('process/*.png'))

    # A list to store our images
    imgs = []

    # Read each image file and add it to imgs
    for image_file in image_files:
        img = imread(image_file)
        parts = image_file.split("/")[1].split(".png")[0].split("_")
        
        
        
        im = plt.imshow(img, animated=True)
        plt.axis('off')  # don't show axes for images
        imgs.append([im]) 

    # Create the animation
    #ani = animation.ArtistAnimation(fig, imgs, interval=1000, blit=True, repeat_delay=1000)

    # Save the animation
    #ani.save("wordcloud_animation.mp4", writer='ffmpeg')

    # Now that we have our animation, we can delete the wordcloud folder.
    #shutil.rmtree('process')
def visualize_jsons(json_input, engagement_name): #Yiqian
    """This takes in a JSON file and creates an mp4 animation of the heatmap.
    Args: JSON file
    Output: An mp4 animation."""

    # Use the open_input function to read the JSON file or list
    data = open_input(json_input)

    # Check if the input data is valid. If not, return an error message
    if valid_json(data) == False:
        return("file input is not valid")
    
    # Define the latitude and longitude
    lat = "lat"
    lon = "lon"
    
    # Create a new list containing only data points with valid latitudes and longitudes
    past_data = data
    data = [element for element in past_data if "tags" in element and lat in element and lon in element and element["timestamp"] >= CUTOFF_DATE]


    def get_geo_coordinate(data):
        lati = []
        long = []
        for element in data:
            lati.append(float(element[lat]))
            long.append(float(element[lon]))
        city_coordinate = [numpy.average(lati), numpy.average(long)]
        return city_coordinate

    def get_month(date):
        date = dateutil.parser.parse(date).date()
        return date.replace(day=1)

    war_data = [item for item in data if item["timestamp"] >= CUTOFF_DATE]
    
    coords={}
    for item in war_data:
        if item.get(lat) and item.get(lon):
            month=get_month(item["timestamp"])
            coords[month]=coords.get(month,[])+[[item[lat],item[lon]]]
    
    city_lat, city_lon = get_geo_coordinate(data)
    
    folder_name = "./heatmaps"
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

    for month, month_data in coords.items():
        city_vis = folium.Map(location=[city_lat, city_lon], zoom_start=13, zoom_control=False)
        folium.TileLayer('stamentoner').add_to(city_vis)
        folium.plugins.HeatMap(month_data, radius=10, max_opacity=0.9).add_to(city_vis)
        html_name = f"{folder_name}/heatmap.html"
        city_vis.save(html_name)
        fileURL = f"file://{os.getcwd()}/{html_name}"
        image_name = f"{folder_name}/Month_of_{month}.png"
        delay = 5
        browser = webdriver.Chrome()
        browser.get(fileURL)
        time.sleep(delay)
        browser.save_screenshot(image_name)
        browser.quit()
        os.remove(html_name)
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ims = []
    image_files = sorted([f"{folder_name}/{img}" for img in os.listdir(folder_name) if img.endswith(".png")])
    start_date_str = image_files[0].split("/")[-1].replace(".png", "").replace("Month_of_", "")
    start_date = datetime.datetime.strptime(start_date_str, "%Y-%m-%d").strftime("%B %Y")
    for image_file in image_files:
        img_np = plt.imread(image_file)
        date_str = image_file.split("/")[-1].replace(".png", "").replace("Month_of_", "")
        date = datetime.datetime.strptime(date_str, "%Y-%m-%d").strftime("%B %Y")
        title = plt.text(0.5, 1.01, f"{engagement_name[0]} \n Engagement started: {start_date} \n {date}", horizontalalignment='center', verticalalignment='bottom', transform=ax.transAxes)
        ims.append([plt.imshow(img_np), title])
    #ani = animation.ArtistAnimation(fig, ims, interval=2000, blit=False, repeat_delay=1000)
    #writer = animation.FFMpegWriter(fps=1, metadata=dict(artist='Me'), bitrate=1800)
    #ani.save("heatmap_animation.mp4", writer=writer)
    #plt.show()

    #shutil.rmtree(folder_name)
def create_combined_animation(heatmap_folder, process_folder, engagement_name): #Yiqian
    # Get sorted list of image files from both folders
    heatmap_images = sorted(glob.glob(f'{heatmap_folder}/*.png'))
    process_images = sorted(glob.glob(f'{process_folder}/*.png'))

    # Make sure we have an equal number of images from both folders
    assert len(heatmap_images) == len(process_images), \
        "The number of images in both folders must be equal"

    # Directory for combined images
    combined_folder = 'combined_images'
    if not os.path.exists(combined_folder):
        os.makedirs(combined_folder)

    # Combine corresponding images from the two folders
    combined_images = []
    for heatmap_image, process_image in zip(heatmap_images, process_images):
        images = [Image.open(x) for x in [heatmap_image, process_image]]
        widths, heights = zip(*(i.size for i in images))

        total_width = sum(widths)
        max_height = max(heights)

        new_img = Image.new('RGB', (total_width, max_height))

        x_offset = 0
        for img in images:
            new_img.paste(img, (x_offset,0))
            x_offset += img.width

        combined_image_path = f'{combined_folder}/{os.path.basename(heatmap_image)}'
        new_img.save(combined_image_path)
        combined_images.append(combined_image_path)

    # Create an animation from the combined images
    fig = plt.figure(figsize=(19.2, 10.8), dpi=100)  # Adjusting figure size and DPI for full screen and high resolution
    ax = fig.add_subplot(111)
    plt.axis('off')
    ims = []
    start_date_str = os.path.basename(combined_images[0]).split('.')[0].replace('Month_of_', '')
    start_date = datetime.datetime.strptime(start_date_str, "%Y-%m-%d").strftime("%B %Y")
    for combined_image in combined_images:
        img = plt.imread(combined_image)
        date_str = os.path.basename(combined_image).split('.')[0].replace('Month_of_', '')
        date = datetime.datetime.strptime(date_str, "%Y-%m-%d").strftime("%B %Y")
        title = plt.text(0.5, 1.01, f"{engagement_name} \n Engagement Started:{start_date} \n {date}", 
                         horizontalalignment='center', verticalalignment='bottom', transform=ax.transAxes)
        ims.append([plt.imshow(img, animated=True), title])

    ani = animation.ArtistAnimation(fig, ims, interval=1000, blit=True, repeat_delay=1000)

    # Save the animation
    ani.save("heatmap_wordcloud_combined.mp4", writer='ffmpeg')
def create_combined_heatmap_wordcloud(input, engagement_name, save_images = False): #Yiqian
    """takes in input data, an engagement name, and a flag for saving intermediate images. 
    It generates word clouds and heatmaps from the input data, then combines these into a synchronized animation. 
    Each frame of the animation corresponds to a specific month and shows the word cloud and heatmap side by side. 
    If the flag for saving images is False (default), all intermediate images used to make the animation are 
    deleted after the final animation is created."""
    create_animations(input, engagement_name)
    visualize_jsons(input, engagement_name)
    create_combined_animation('heatmaps', 'process', engagement_name)
    if save_images == False:
        shutil.rmtree("heatmaps")
        shutil.rmtree("process")
        shutil.rmtree("combined_images")
