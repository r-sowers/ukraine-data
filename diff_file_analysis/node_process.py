# Node History Files Compressing Script
# Professor Richard Sowers
# Members: Venkata Sai Narayana Bavisetty, Yuxuan Chen, Paul Ge, Yujie Miao, Yiqian Zhang, Runlin Zheng

import json
import requests
import xmltodict
import sys
import os
FOLDER_PATH = "./avdiivka"

# 20 randomly selected node ids from Avdiivka database for testing purposes
# 9466394691 2503702640 1601320528 1619789064 9507132589 1489676606 10864855641 10753498547 2061480781 9466394700
# 9466394692 9466394699 1603236339 2511524960 1602504516 2061480784 3880331510  1601877790  1603175151 1602060344

if __name__ == "__main__":
    data_json_all = []
    # Iterate over files in the folder
    for file_name in os.listdir(FOLDER_PATH):
        if file_name.endswith(".txt"):
            file_path = os.path.join(FOLDER_PATH, file_name)
            with open(file_path, "r", encoding="utf-8") as file:
                data_text = file.read()
                data_json = json.loads(data_text)
                # print(data_json[0])
                data_json_all.append(data_json[0])
                # print(len(data_json_all))
    with open(f"{FOLDER_PATH}/20_node_histories.json", "w", encoding="utf-8") as file:
        json.dump(data_json_all, file, indent=4, ensure_ascii=False)
