[
  {
    "id": "2511524960",
    "visible": "true",
    "version": "4",
    "changeset": "121539658",
    "timestamp": "2022-05-26T18:06:20Z",
    "user": "andygol",
    "uid": "94578",
    "lat": "48.1261469",
    "lon": "37.7390258",
    "tag": [
      {
        "k": "entrance",
        "v": "main"
      },
      {
        "k": "name",
        "v": "Аис-Т"
      },
      {
        "k": "opening_hours",
        "v": "08:00-18:00"
      },
      {
        "k": "phone",
        "v": "+380 50 511 9847;+380 6236 33 005"
      },
      {
        "k": "shop",
        "v": "car_parts"
      }
    ]
  },
  {
    "id": "2511524960",
    "visible": "true",
    "version": "5",
    "changeset": "133273589",
    "timestamp": "2023-03-03T23:33:10Z",
    "user": "avinet_ua",
    "uid": "1538111",
    "lat": "48.1261469",
    "lon": "37.7390258",
    "tag": [
      {
        "k": "entrance",
        "v": "main"
      },
      {
        "k": "name",
        "v": "Аис-Т"
      },
      {
        "k": "name:uk",
        "v": "Аис-Т"
      },
      {
        "k": "opening_hours",
        "v": "08:00-18:00"
      },
      {
        "k": "phone",
        "v": "+380 50 511 9847;+380 6236 33 005"
      },
      {
        "k": "shop",
        "v": "car_parts"
      }
    ]
  }
]