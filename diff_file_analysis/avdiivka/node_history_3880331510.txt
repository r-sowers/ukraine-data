[
  {
    "id": "3880331510",
    "visible": "true",
    "version": "2",
    "changeset": "132814376",
    "timestamp": "2023-02-21T01:46:30Z",
    "user": "avinet_ua",
    "uid": "1538111",
    "lat": "48.1342812",
    "lon": "37.7403464",
    "tag": [
      {
        "k": "barrier",
        "v": "gate"
      },
      {
        "k": "bicycle",
        "v": "yes"
      },
      {
        "k": "foot",
        "v": "yes"
      },
      {
        "k": "horse",
        "v": "yes"
      },
      {
        "k": "motor_vehicle",
        "v": "yes"
      },
      {
        "k": "name",
        "v": "Ворота в садок \"Альонка\""
      },
      {
        "k": "name:uk",
        "v": "Ворота в садок \"Альонка\""
      }
    ]
  }
]