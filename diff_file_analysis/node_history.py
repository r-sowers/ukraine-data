# Node History Script
# Professor Richard Sowers
# Members: Venkata Sai Narayana Bavisetty, Yuxuan Chen, Paul Ge, Yujie Miao, Yiqian Zhang, Runlin Zheng

import json
import requests
import xmltodict
import sys
import datetime
START_TIME = "2021-11-01T00:00:00Z"


def history_scraper(node_id):
    url = f"https://www.openstreetmap.org/api/0.6/node/{node_id}/history"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.content.decode("utf-8")
        json_data = json.loads(json.dumps(xmltodict.parse(data), ensure_ascii=False))
        # Extract the "node" part from the JSON data
        json_data = json.dumps(json_data['osm']['node'], ensure_ascii=False)
        # Parse JSON data
        data = json.loads(json_data)
        # Remove "@" signs from keys
        modified_data = remove_at_signs(data)
        output = []
        # data could be a list of dics, or simply a dic.
        if isinstance(data, list):
            for part in modified_data:
                print(part["timestamp"])
                if check_timestamp(part["timestamp"]): output.append(part)
        else:
            if check_timestamp(modified_data["timestamp"]): output.append(modified_data)
        # Convert the modified data back to JSON
        json_output = json.dumps(output, indent=2, ensure_ascii=False)
        print(json_output)
        print(type(json_output))
        file_name = f"./avdiivka/node_history_{node_id}.txt"
        with open(file_name, "w", encoding='utf-8') as f1:
            f1.write(json_output)
        print(f"node {node_id} success")
    else:
        print("Request failed with status code:", response.status_code)


# Function to remove "@" signs from keys recursively
def remove_at_signs(obj):
    if isinstance(obj, list):
        return [remove_at_signs(item) for item in obj]
    elif isinstance(obj, dict):
        return {key.replace("@", ""): remove_at_signs(value) for key, value in obj.items()}
    else:
        return obj
    
# checks the timestamp.
# Returns 1 (true) if timestamp is newer than the START_TIME.
def check_timestamp(time_str):
    # Convert the datetime string to a datetime object
    time_obj = datetime.datetime.strptime(time_str, "%Y-%m-%dT%H:%M:%SZ")
    start_time_obj = datetime.datetime.strptime(START_TIME, "%Y-%m-%dT%H:%M:%SZ")
    if time_obj > start_time_obj:
        return 1
    return 0
    

# 20 randomly selected node ids from Avdiivka database for testing purposes, can directly copy
# 9466394691 2503702640 1601320528 1619789064 9507132589 1489676606 10864855641 10753498547 2061480781 9466394700
# 9466394692 9466394699 1603236339 2511524960 1602504516 2061480784 3880331510  1601877790  1603175151 1602060344
if __name__ == "__main__":

    if len(sys.argv) <= 1:
        print("Incorrect usage")
        print("Please do: python node_history.py [node_id_1] [node_id_2] ...")
        print("Example1:  python node_history.py 466394691 2503702640")
        print("Example2:  python node_history.py 1619789064 9507132589 1489676606 10864855641")
        sys.exit(0)
    
    for node_id in sys.argv[1:]:   
        try:
            history_scraper(node_id)
        except:
            print(f"Error: node_id {node_id} failed.\n")